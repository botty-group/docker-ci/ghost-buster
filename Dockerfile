FROM debian

ENV DEBIAN_FRONTEND=noninteractive
ARG DOCKER_MACHINE_VERSION=0.16.2

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y wget && \
    apt-get install -y git && \
    apt-get install -y python && \
    apt-get install -y python-pip
RUN python --version && \
    pip install buster

ENTRYPOINT ["/usr/local/bin/buster"]